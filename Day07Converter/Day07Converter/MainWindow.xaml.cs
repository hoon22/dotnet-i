﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Converter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConvertCtoF()
        {
            if (!double.TryParse(tbCelsius.Text, out double celcius))
            {
                //lbFahrenheit.Content = "[ERROR] Invalid type";
                MessageBox.Show("[ERROR] Invalid type");
                return;
            }

            double fahrenheit = (celcius * 9 / 5) + 32;

            lblFahrenheit.Content = string.Format("{0:0.#} Fahrenheit", fahrenheit);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ConvertCtoF();
        }

        private void TblCelsius_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ConvertCtoF();
            }
        }
    }
}
