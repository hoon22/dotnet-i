﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08PrimePalGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool IsPrimeNumber(int number)
        {
            if (number < 2) return false;

            // only need to compare odd number
            for (int i = 2; i <= number / 2; i++)  
            {
                if (number % i == 0)    // can be divided other number
                {
                    return false;
                }
            }
            return true;
        }

        private string ReverseString(string str)
        {
            if (str == null) return null;

            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);

            return new string(charArray);
        }

        private bool IsPalindromicNumber(int number)
        {
            if (number < 0) return false;   // number should be greater than 0
            if (number < 10) return true;   // 1 digit number is a palindromic number

            string numberStr = number.ToString();

            string substring1 = numberStr.Substring(0, numberStr.Length / 2);
            string substring2;
            // convert half of string to reverse
            if (numberStr.Length % 2 == 0)  // Length is even number.
            {
                substring2 = ReverseString(numberStr.Substring(numberStr.Length / 2));     // startIndex is middle
            }
            else
            {
                substring2 = ReverseString(numberStr.Substring(numberStr.Length / 2 + 1));
            }

            // palindronic number
            if (substring1 == substring2) return true;

            return false;
        }

        private void ButtonGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(tbStartNumber.Text, out int startNumber) || startNumber < 1)
            {
                MessageBox.Show("[Error] Invalid Input");
                return;
            }

            if (!int.TryParse(tbEndNumber.Text, out int endNumber) || endNumber < 1)
            {
                MessageBox.Show("[Error] Invalid Input");
                return;
            }

            if (startNumber > endNumber)
            {
                MessageBox.Show("[Error] Invalid Input. The 1st number should be greather than or equal to the 2nd number.");
                return;
            }

            List<int> resultList = new List<int>();
            for (int i = startNumber; i <= endNumber; i++)
            {
                // check if number is prime number
                if (!IsPrimeNumber(i)) continue;

                // if palindromic number is not
                if ((cbPalPrime.IsChecked == true) && !IsPalindromicNumber(i)) continue;

                // Add to the list
                resultList.Add(i);
            }

            tblResult.Text = string.Format("Result: {0} numbers{1}{2}", resultList.Count, Environment.NewLine, string.Join(",", resultList));
        }
    }
}
