﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numList = new List<int>();

            const int minValue = -100;
            const int maxValue = 100 + 1;

            int countOfNumbers = 0;

            try
            {
                Console.Write("How many numbers you want to generate ? ");
                if (!int.TryParse(Console.ReadLine(), out countOfNumbers)) {
                    Console.WriteLine("Error: Not a valid number.");
                    return;
                }

                Random random = new Random();
                for (int i = 0; i < countOfNumbers; i++)
                {
                    numList.Add(random.Next(minValue, maxValue));
                }

                for (int i = 0; i < numList.Count; i++)
                {
                    if (numList[i] <= 0)
                    {
                        Console.WriteLine(numList[i]);
                    }
                }
            }
            finally
            {
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }
    }
}
