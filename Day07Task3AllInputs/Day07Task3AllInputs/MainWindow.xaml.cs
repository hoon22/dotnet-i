﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Task3AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonRegisterMe_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(tbName.Text, @"^[^;]{1,}$"))
            {
                MessageBox.Show("[ERROR] Name must not be empty, semicolon(;) not allowed.");
                return;
            }

            string ageStr;
            if (rbAge1.IsChecked == true)
            {
                ageStr = rbAge1.Content.ToString();
            }
            else if (rbAge2.IsChecked == true)
            {
                ageStr = rbAge2.Content.ToString();
            }
            else if (rbAge3.IsChecked == true)
            {
                ageStr = rbAge3.Content.ToString();
            }
            else
            {
                MessageBox.Show("[ERROR] Internal system error");
                return;
            }

            /*
            string pets;
            pets = ((ckbCat.IsChecked == true) ? ckbCat.Content.ToString() : "");
            if (ckbDog.IsChecked == true) {
                pets += ((pets != "") ? "," : "") + ckbDog.Content.ToString();
            }
            if (ckbOthers.IsChecked == true)
            {
                pets += ((pets != "") ? "," : "") + ckbOthers.Content.ToString();
            }
            */
            // Using string.Join()
            List<string> petList = new List<string>();
            if (ckbCat.IsChecked == true)
            {
                petList.Add(ckbCat.Content.ToString());
            }
            if (ckbDog.IsChecked == true)
            {
                petList.Add(ckbDog.Content.ToString());
            }
            if (ckbOther.IsChecked == true)
            {
                petList.Add(ckbOther.Content.ToString());
            }
            string petStr = string.Join(",", petList);

            // save data
            string data = string.Format("{0};{1};{2};{3};{4}{5}", tbName.Text, ageStr, petStr, comboContinent.Text, lblTemp.Content, Environment.NewLine);
            File.AppendAllText(@"data.txt", data);

            MessageBox.Show("Successfully registered!");
        }

        private void SlValue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblTemp.Content = string.Format("{0} C", slTemp.Value);
        }
    }
}
