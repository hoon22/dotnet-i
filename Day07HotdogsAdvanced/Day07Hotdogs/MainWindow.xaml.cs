﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Hotdogs
{
    public class Hotdog
    {
        public Hotdog(string name, string birthDate, string bunType, string sausageType, int temperature)
        {
            Name = name;
            BirthDate = birthDate;
            BunType = bunType;
            SausageType = sausageType;
            Temperature = temperature;
        }

        private string _name;
        public string Name
        {
            get { return _name;  }
            set
            {
                if (!Regex.IsMatch(value, @"^[^;]{2,20}$"))
                {
                    throw new InvalidDataException("[ERROR] Name should be 2-20 characters long, not allowed semicolon");
                }
                _name = value;
            }
        }

        private string _birthDate;
        public string BirthDate
        {
            get
            {
                return _birthDate;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"))
                {
                    throw new InvalidDataException("Invalid type");
                }

                DateTime birthday = DateTime.Parse(value);
                DateTime now = DateTime.Now;

                // calculate time difference
                long diffDate = now.Subtract(birthday).Ticks;
                // convert time to year
                int years = new DateTime(diffDate).Year - 1;

                if (years < 0 || 150 < years)
                {
                    throw new InvalidDataException("[ERROR] Age should be 0 - 150 y/o.");
                }
                _birthDate = value;
            }
        }

        public string _bunType;
        public string BunType
        {
            get { return _bunType; }
            set
            {
                string[] bunTypeArray = { "regular", "whole grain", "italian" };
                if (!bunTypeArray.Contains(value))
                {
                    throw new InvalidDataException("[ERROR] Invalid bun type " + value);
                }
                _bunType = value;
            }
        }

        public string _sausageType;
        public string SausageType
        {
            get { return _sausageType; }
            set
            {
                string[] sausageTypeArray = { "German", "with cheese", "vegetarian" };
                if (!sausageTypeArray.Contains(value))
                {
                    throw new InvalidDataException("[ERROR] Invalid sausage type " + value);
                }
                _sausageType = value;
            }
        }

        public int _temperature;
        public int Temperature
        {
            get { return _temperature; }
            set
            {
                if (value < 40 || 90 < value)
                {
                    throw new InvalidDataException("[ERROR] Invalid temperature value " + value);
                }
                _temperature = value;
            }
        }

        public string[] Toppings;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static private List<Hotdog> hotdogsList = new List<Hotdog>();
        private int index;      // index of list<Hotdog>, it will be used to navigate information.

        public MainWindow()
        {
            InitializeComponent();
        }

        // Display user information
        void DisplayInformation()
        {
            tbCustomerName.Text = hotdogsList[index].Name;
            tbDateOfBirth.Text = hotdogsList[index].BirthDate;

            switch (hotdogsList[index].BunType)
            {
                case "regular":
                    rbRegular.IsChecked = true;
                    break;
                case "whole grain":
                    rbWholeGrain.IsChecked = true;
                    break;
                case "italian":
                    rbItalian.IsChecked = true;
                    break;
                default:
                    MessageBox.Show("Internal Error");
                    break;
            }

            switch (hotdogsList[index].SausageType)
            {
                case "German":
                    rbGerman.IsChecked = true;
                    break;
                case "with cheese":
                    rbWithCheese.IsChecked = true;
                    break;
                case "vegetarian":
                    rbVegetarian.IsChecked = true;
                    break;
                default:
                    MessageBox.Show("Internal Error");
                    break;
            }

            slTempCelsius.Value = hotdogsList[index].Temperature;

            // initialize value
            cbKetchup.IsChecked = false;
            cbMustard.IsChecked = false;
            cbRelish.IsChecked = false;
            // if topping list is empty,
            if (hotdogsList[index].Toppings == null) return;

            foreach (string topping in hotdogsList[index].Toppings)
            {
                switch (topping)
                {
                    case "Ketchup":
                        cbKetchup.IsChecked = true;
                        break;
                    case "Mustard":
                        cbMustard.IsChecked = true;
                        break;
                    case "Relish":
                        cbRelish.IsChecked = true;
                        break;
                    default:
                        MessageBox.Show("Internal Error");
                        break;
                }
            }
        }

        private void ButtonPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Regex.IsMatch(tbCustomerName.Text, @"^[^;]{2,20}$"))
                {
                    MessageBox.Show("[ERROR] Name should be 2-20 characters long, not allowed semicolon");
                    return;
                }

                if (!Regex.IsMatch(tbDateOfBirth.Text, @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"))
                {
                    MessageBox.Show("[ERROR] Invalid data of birth format");
                    return;
                }

                DateTime birthday = DateTime.Parse(tbDateOfBirth.Text);
                DateTime now = DateTime.Now;

                // calculate time difference
                long diffDate = now.Subtract(birthday).Ticks;
                // convert time to year
                int years = new DateTime(diffDate).Year - 1;

                if (years < 0 || 150 < years)
                {
                    MessageBox.Show("[ERROR] Age should be 0 - 150 y/o.");
                    return;
                }

                string bunType;
                if (rbRegular.IsChecked == true)
                {
                    bunType = "regular"; //rbRegular.Content.ToString();     // get value from control
                }
                else if (rbWholeGrain.IsChecked == true)
                {
                    bunType = rbWholeGrain.Content.ToString();
                }
                else if (rbItalian.IsChecked == true)
                {
                    bunType = rbItalian.Content.ToString();
                }
                else
                {
                    MessageBox.Show("[ERROR] Internal System Error!");
                    return;
                }

                string sausageType;
                if (rbGerman.IsChecked == true)
                {
                    sausageType = rbGerman.Content.ToString();     // get value from control
                }
                else if (rbWithCheese.IsChecked == true)
                {
                    sausageType = rbWithCheese.Content.ToString();
                }
                else if (rbVegetarian.IsChecked == true)
                {
                    sausageType = rbVegetarian.Content.ToString();
                }
                else
                {
                    MessageBox.Show("[ERROR] Internal System Error!");
                    return;
                }

                List<string> toppingList = new List<string>();
                if (cbKetchup.IsChecked == true)
                {
                    toppingList.Add(cbKetchup.Content.ToString());
                }
                if (cbMustard.IsChecked == true)
                {
                    toppingList.Add(cbMustard.Content.ToString());
                }
                if (cbRelish.IsChecked == true)
                {
                    toppingList.Add(cbRelish.Content.ToString());
                }
                string toppings = string.Join(",", toppingList);

                // make string to save data
                string saveData = string.Format("{0};{1};{2};{3};{4};{5}{6}", 
                                    tbCustomerName.Text, tbDateOfBirth.Text, bunType, sausageType, slTempCelsius.Value, toppings, Environment.NewLine);

                // save data
                File.AppendAllText("hotdogs.txt", saveData);
                index = 0;

                MessageBox.Show("Data was saved successfully.");
            }
            catch (FormatException ex)
            {
                MessageBox.Show("[ERROR] Date Format Exception: " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("[ERROR] Date of birth must be before than today: " + ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to save data: " + ex.Message);
            }
        }

        private void ButtonReadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                hotdogsList.Clear();

                string[] lineArray = File.ReadAllLines(@"hotdogs.txt");

                foreach (string line in lineArray)
                {
                    string[] data = line.Split(';');

                    string name = data[0];
                    string dateOfBirth = data[1];
                    string bunType = data[2];
                    string sausageType = data[3];
                    int temperature;
                    if (!int.TryParse(data[4], out temperature))
                    {
                        MessageBox.Show("Invalid temperature.");
                        return;
                    }

                    string[] toppingArray = null;
                    if (data[5] != "")
                    {
                        toppingArray = data[5].Split(',');
                    }

                    Hotdog hotdog = new Hotdog(name, dateOfBirth, bunType, sausageType, temperature);
/*
                    hotdog.Name = name;
                    hotdog.BirthDate = dateOfBirth;
                    hotdog.BunType = bunType;
                    hotdog.SausageType = sausageType;
                    hotdog.Temperature = temperature;
*/  
                    hotdog.Toppings = toppingArray;
                    // Add to list
                    hotdogsList.Add(hotdog);
                }
                // display first item
                DisplayInformation();
                MessageBox.Show("Read data successfully");
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SlTempCelsius_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblTemp.Content = slTempCelsius.Value;
        }

        private void ButtonPre_Click(object sender, RoutedEventArgs e)
        {
            if (hotdogsList.Count == 0)
            {
                MessageBox.Show("No data avaliable");
                return;
            }

            // decrease index
            if (index > 0) index = index - 1;

            DisplayInformation();
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if (hotdogsList.Count == 0)
            {
                MessageBox.Show("No data avaliable");
                return;
            }

            // increase index
            if (index < hotdogsList.Count - 1) index = index + 1;

            DisplayInformation();
        }

        private void ButtonModify_Click(object sender, RoutedEventArgs e)
        {
            if (hotdogsList.Count() <= 0) return;

            if (!Regex.IsMatch(tbCustomerName.Text, @"^[^;]{2,20}$"))
            {
                MessageBox.Show("[ERROR] Name should be 2-20 characters long, not allowed semicolon");
                return;
            }

            if (!Regex.IsMatch(tbDateOfBirth.Text, @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"))
            {
                MessageBox.Show("[ERROR] Invalid data of birth format");
                return;
            }

            DateTime birthday = DateTime.Parse(tbDateOfBirth.Text);
            DateTime now = DateTime.Now;

            // calculate time difference
            long diffDate = now.Subtract(birthday).Ticks;
            // convert time to year
            int years = new DateTime(diffDate).Year - 1;

            if (years < 0 || 150 < years)
            {
                MessageBox.Show("[ERROR] Age should be 0 - 150 y/o.");
                return;
            }

            string bunType;
            if (rbRegular.IsChecked == true)
            {
                bunType = "regular"; //rbRegular.Content.ToString();     // get value from control
            }
            else if (rbWholeGrain.IsChecked == true)
            {
                bunType = rbWholeGrain.Content.ToString();
            }
            else if (rbItalian.IsChecked == true)
            {
                bunType = rbItalian.Content.ToString();
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error!");
                return;
            }

            string sausageType;
            if (rbGerman.IsChecked == true)
            {
                sausageType = rbGerman.Content.ToString();     // get value from control
            }
            else if (rbWithCheese.IsChecked == true)
            {
                sausageType = rbWithCheese.Content.ToString();
            }
            else if (rbVegetarian.IsChecked == true)
            {
                sausageType = rbVegetarian.Content.ToString();
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error!");
                return;
            }

            List<string> toppingList = new List<string>();
            if (cbKetchup.IsChecked == true)
            {
                toppingList.Add(cbKetchup.Content.ToString());
            }
            if (cbMustard.IsChecked == true)
            {
                toppingList.Add(cbMustard.Content.ToString());
            }
            if (cbRelish.IsChecked == true)
            {
                toppingList.Add(cbRelish.Content.ToString());
            }

            hotdogsList[index].Name = tbCustomerName.Text;
            hotdogsList[index].BirthDate = tbDateOfBirth.Text;
            hotdogsList[index].BunType = bunType;
            hotdogsList[index].SausageType = sausageType;
            hotdogsList[index].Temperature = (int)slTempCelsius.Value;
            hotdogsList[index].Toppings = toppingList.ToArray();


            List<string> saveData = new List<string>();
            foreach (Hotdog hotdog in hotdogsList)
            {
                string line;

                line = string.Format("{0};{1};{2};{3};{4};{5}",
                    hotdog.Name, hotdog.BirthDate, hotdog.BunType, hotdog.SausageType, hotdog.Temperature,
                    (hotdog.Toppings == null ? "" : string.Join(",", hotdog.Toppings))
                );
                saveData.Add(line);
            }

            File.WriteAllLines(@"hotdogs.txt", saveData);
        }
    }
}
