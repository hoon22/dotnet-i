﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Find
{
    class Program
    {
        const int numberOfInput = 5;

        static double sum;
        static double avg;

        private static double[] userInput;

        // List type
        static List<double> dataList = new List<double>();

        static bool EnterValues()
        {
            Console.WriteLine("Please enter {0} floating point value.", numberOfInput);

            // get the values from user
            for (int i = 0; i < numberOfInput; i++)
            {
                Console.Write("Value {0}: ", i + 1);
                if (!double.TryParse(Console.ReadLine(), out userInput[i]))
                {
                    Console.WriteLine("Error: Value entered must be a float number.");
                    return false;
                }
                // using list
                dataList.Add(userInput[i]);
            }
            return true;
        }

        static double FindSmallestNumber()
        {
            // using array
            double smallest = userInput[0];

            for (int i = 0; i < numberOfInput; i++)
            {
                if (smallest > userInput[i])
                {
                    smallest = userInput[i];
                }
            }

            // using list
            double small = double.MaxValue;
            foreach (var value in dataList)
            {
                if (small > value)
                {
                    small = value;
                }
            }

            // using array.ForEach method
            small = double.MaxValue;
            dataList.ForEach(delegate (double value)
            {
                if (small > value)
                {
                    small = value;
                }
            });

//            return smallest;
            return small;
        }

        static double FindSumOfAllNumbers()
        {
            sum = 0;
            foreach (double value in userInput)
            {
                sum += value;
            }
            return sum;
        }

        static double FindAverage()
        {
            avg = sum / numberOfInput;

            return avg;
        }

        static double FindStandardDeviation()
        {
            double sumOfsquaredDiff = 0;
            foreach (double value in userInput)
            {
                sumOfsquaredDiff += Math.Pow(value - avg, 2);
            }

            return Math.Sqrt(sumOfsquaredDiff / numberOfInput);
        }

        static void Main(string[] args)
        {
            userInput = new double[numberOfInput];

            if (EnterValues())
            {
                // if all of the 5 user input is valid, do calculation.
                Console.WriteLine("The smallest of the numbers: {0}", FindSmallestNumber());
                Console.WriteLine("The sum of all the numbers: {0}", FindSumOfAllNumbers());
                Console.WriteLine("The average of all the those values: {0}", FindAverage());
                Console.WriteLine("Standard deviation of those values: {0}", FindStandardDeviation());
            }

            Console.WriteLine("\nPress any key to exit");
            Console.ReadKey();
        }
    }
}
