﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day05AirportTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string message) : base(message) { }
    }

    class Airport
    {
        public Airport(string code, string city, double latitude, double logitude)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = logitude;
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$")) {
                    throw new InvalidDataException("[Error] Code should be 3 upper-case letters. : " + value);
                }
                _code = value;
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                if (value == "")
                {
                    throw new InvalidDataException("[Error] City must not be empty.");
                }
                _city = value;
            }
        }

        private double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if ((value < -90.0) || (90.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of latitudes should be from -90 to 90. : " + value);
                }
                _latitude = value;
            }
        }

        private double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if ((value < -180.0) || (180.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of longitudes should be from -180 to 180. : " + value);
                }
                _longitude = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}:{1} Airport - Latitude: {2}, Longitude: {3}", Code, City, Latitude, Longitude);
        }
    }

    class Program
    {
        // data file path
        const string FILE_PATH = @"..\..\..\airports.txt";
        // list of Airport object
        static List<Airport> airportList = new List<Airport>();

        // Display user command menu
        static void DisplayMenu()
        {
            Console.WriteLine(
                "\n1. Show all airports (codes and city names)\n" +
                "2. Find distance between two airports by codes.\n" +
                "3. Find the airports nearest to an airport given by code and display the distance.\n" +
                "4. Add a new airport to the list.\n" +
                "5. Delete an airport from the list.\n" +
                "0. Exit"
            );
            Console.Write("Choice: ");
        }

        // if command is invalid, return -1. otherwise return value 0 - 4
        static int GetUserCommand()
        {
            DisplayMenu();

            if (!int.TryParse(Console.ReadLine(), out int command))
            {
                return -1;  // error
            }
            return command;
        }

        static void ReadAllAirportFromFile()
        {
            try
            {
                string[] lineArray = File.ReadAllLines(FILE_PATH);

                foreach (string line in lineArray)
                {
                    string[] data = line.Split(';');

                    if (data.Length != 4)
                    {
                        Console.WriteLine("[ERROR] Corrupted data: {0}", line);
                        continue;
                    }

                    string code = data[0];
                    string city = data[1];
                    
                    if (!double.TryParse(data[2], out double latitude))
                    {
                        Console.WriteLine("[ERROR] Corrupted data, Invalid data type of latitude: {0}", line);
                        continue;
                    }

                    if (!double.TryParse(data[3], out double longitude))
                    {
                        Console.WriteLine("[ERROR] Corrupted data, Invalid data type of longitude: {0}", line);
                        continue;
                    }

                    try
                    {
                        airportList.Add(new Airport(code, city, latitude, longitude));
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("{0}: {1}", ex.Message, line);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Fail to read data from file. " + ex.Message);
            }
        }

        static void SaveAllAirportToFile()
        {
            List<string> airportStrList = new List<string>();

            // Create string list to save data
            foreach (Airport airport in airportList)
            {
                airportStrList.Add(string.Format("{0};{1};{2};{3}", airport.Code, airport.City, airport.Latitude, airport.Longitude));
            }

            // Save data to file.
            try
            {
                File.WriteAllLines(FILE_PATH, airportStrList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Fail to save data into file. " + ex.Message);
            }
        }

        static void ShowAllAirportInfo()
        {
            Console.WriteLine("\n-------------- Airport list --------------");
            foreach (Airport airport in airportList)
            {
                Console.WriteLine(airport);
            }
        }

        // this method referenced from stackoverflow
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }

        static bool FindAirportByCode(string code, out Airport airport)
        {
            bool isFound = false;

            airport = null;     // if not found, it will return null.

            foreach (Airport a in airportList)
            {
                if (code == a.Code)
                {
                    isFound = true;
                    airport = a;    // assign airport value
                    break;
                }
            }
            return isFound;
        }

        static void FindDistanceByCodes()
        {
            Console.Write("Airport Code 1: ");
            if (!FindAirportByCode(Console.ReadLine(), out Airport a1))
            {
                Console.WriteLine("[ERROR] Code does not exist in the airport list");
                return;
            }

            Console.Write("Airport Code 2: ");
            if (!FindAirportByCode(Console.ReadLine(), out Airport a2))
            {
                Console.WriteLine("[ERROR] Code does not exist in the airport list");
                return;
            }

            Console.WriteLine("\nDistance from {0}({1}) to {2}({3}) : {4:0.00} Km",
                a1.Code, a1.City, a2.Code, a2.City,
                DistanceTo(a1.Latitude, a1.Longitude, a2.Latitude, a2.Longitude)
            );
        }

        static void FindNearestAirport()
        {
            Console.Write("Airport Code: ");
            if (!FindAirportByCode(Console.ReadLine(), out Airport from))
            {
                Console.WriteLine("[ERROR] Code does not exist in the airport list");
                return;
            }

            double nearestDistance = double.MaxValue;    // set as maximum value
            Airport nearestAirport = null;

            foreach (Airport to in airportList)
            {
                if (from.Code == to.Code)
                {
                    // same airport, don't need to compare
                    continue;
                }
                double distance = DistanceTo(from.Latitude, from.Longitude, to.Latitude, to.Longitude);

                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestAirport = to;
                }
            }

            if (nearestAirport == null)
            {
                Console.WriteLine("\n[ERROR] Only 1 airport is in the list. Can't find nearest other airport");
            }
            else
            {
                Console.WriteLine("\n{0}({1}) is the nearest airport from {2}({3}).\nDistance is {4:0.00}Km.", 
                    nearestAirport.Code, nearestAirport.City, from.Code, from.City, nearestDistance);
            }
        }

        static void AddNewAirport()
        {
            Console.WriteLine("\nPlease enter the airport information:");
            Console.Write("Code: ");
            string code = Console.ReadLine();

            Console.Write("City: ");
            string name = Console.ReadLine();

            Console.Write("Latitude: ");
            if (!double.TryParse(Console.ReadLine(), out double latitude))
            {
                Console.WriteLine("[ERROR] Invalid data type of latitude. It should be double.");
                return;
            }

            Console.Write("Longitude: ");
            if (!double.TryParse(Console.ReadLine(), out double longitude))
            {
                Console.WriteLine("[ERROR] Invalid data type of latitude. It should be double.");
                return;
            }

            try
            {
                // Create new Airport object and add it into the list
                airportList.Add(new Airport(code, name, latitude, longitude));
                Console.WriteLine("New airport added successfully.");
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void DeleteAirportByCode()
        {
            Console.Write("Airport Code: ");
            if (!FindAirportByCode(Console.ReadLine(), out Airport airportToDelete))
            {
                Console.WriteLine("[ERROR] Code does not exist in the airport list");
                return;
            }

            foreach (var a in airportList)
            {
                if (a == airportToDelete)
                {
                    airportList.Remove(a);
                    Console.WriteLine("\nAirport was successfully deleted from the list.");
                    break;
                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // Read data from file
                ReadAllAirportFromFile();

                int command;
                do
                {
                    command = GetUserCommand();
                    switch (command)
                    {
                        case 0:
                            break;
                        case 1:
                            ShowAllAirportInfo();
                            break;
                        case 2:
                            FindDistanceByCodes();
                            break;
                        case 3:
                            FindNearestAirport();
                            break;
                        case 4:
                            AddNewAirport();
                            break;
                        case 5:
                            DeleteAirportByCode();
                            break;
                        default:
                            Console.WriteLine("Invalid command try again.\n");
                            break;
                    }
                } while (command != 0);

                // Save result
                SaveAllAirportToFile();
                Console.WriteLine("\nGood bye!\n");
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
