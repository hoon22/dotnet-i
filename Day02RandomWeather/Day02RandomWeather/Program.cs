﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02RandomWeather
{
    class Program
    {
        static void Main(string[] args)
        {
            const int lowestTemp = -30;
            const int highestTemp = 30;

            Random random = new Random();
            int temperature;

            temperature = random.Next(lowestTemp, highestTemp + 1);

            Console.WriteLine("Temperature: {0}", temperature);
            if (temperature > 15)
            {
                Console.WriteLine("That's what I like");
            }
            else
            {
                if (temperature >= 0)
                {
                    Console.WriteLine("Spring or Fall");
                }
                else
                {
                    if (temperature > -15)
                    {
                        Console.WriteLine("Freezing already");
                    }
                    else
                    {
                        Console.WriteLine("Very very cold");
                    }
                }
            }

            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}
