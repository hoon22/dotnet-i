﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Hotdogs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Regex.IsMatch(tbCustomerName.Text, @"^[^;]{2,20}$"))
                {
                    MessageBox.Show("[ERROR] Name should be 2-20 characters long, not allowed semicolon");
                    return;
                }

                if (!Regex.IsMatch(tbDateOfBirth.Text, @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"))
                {
                    MessageBox.Show("[ERROR] Invalid data of birth format");
                    return;
                }

                DateTime birthday = DateTime.Parse(tbDateOfBirth.Text);
                DateTime now = DateTime.Now;

                // calculate time difference
                long diffDate = now.Subtract(birthday).Ticks;
                // convert time to year
                int years = new DateTime(diffDate).Year - 1;

                if (years < 0 || 150 < years)
                {
                    MessageBox.Show("[ERROR] Age should be 0 - 150 y/o.");
                    return;
                }

                string bunType;
                if (rbRegular.IsChecked == true)
                {
                    bunType = rbRegular.Content.ToString();     // get value from control
                }
                else if (rbWholeGrain.IsChecked == true)
                {
                    bunType = rbWholeGrain.Content.ToString();
                }
                else if (rbItalian.IsChecked == true)
                {
                    bunType = rbItalian.Content.ToString();
                }
                else
                {
                    MessageBox.Show("[ERROR] Internal System Error!");
                    return;
                }

                string sausageType;
                if (rbGerman.IsChecked == true)
                {
                    sausageType = rbGerman.Content.ToString();     // get value from control
                }
                else if (rbWithCheese.IsChecked == true)
                {
                    sausageType = rbWithCheese.Content.ToString();
                }
                else if (rbVegetarian.IsChecked == true)
                {
                    sausageType = rbVegetarian.Content.ToString();
                }
                else
                {
                    MessageBox.Show("[ERROR] Internal System Error!");
                    return;
                }

                List<string> toppingList = new List<string>();
                if (cbKetchup.IsChecked == true)
                {
                    toppingList.Add(cbKetchup.Content.ToString());
                }
                if (cbMustard.IsChecked == true)
                {
                    toppingList.Add(cbMustard.Content.ToString());
                }
                if (cbRelish.IsChecked == true)
                {
                    toppingList.Add(cbRelish.Content.ToString());
                }
                string toppings = string.Join(",", toppingList);

                // make string to save data
                string saveData = string.Format("{0};{1};{2};{3};{4};{5}{6}", 
                                    tbCustomerName.Text, tbDateOfBirth.Text, bunType, sausageType, slTempCelsius.Value, toppings, Environment.NewLine);

                // save data
                File.AppendAllText("hotdogs.txt", saveData);
                MessageBox.Show("Data was saved successfully.");
            }
            catch (FormatException ex)
            {
                MessageBox.Show("[ERROR] Date Format Exception: " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("[ERROR] Date of birth must be before than today: " + ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to save data: " + ex.Message);
            }
        }
    }
}
