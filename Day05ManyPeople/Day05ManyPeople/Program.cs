﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05ManyPeople
{
    class Person
    {
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        private string _name;
        private int _age;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if ((value < 0) || (150 < value))
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150.");
                }
                _age = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} is {1} y/o.", Name, Age);
        }
    }

    class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            this.Program = program;
            this.GPA = gpa;
        }

        private string _program;
        private double _gpa;

        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 2 || 100 < value.Length)
                {
                    throw new ArgumentOutOfRangeException("Program name must be between 2 and 100 characters long");
                }
                _program = value;
            }
        }

        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0 || 4.0 < value)
                {
                    throw new ArgumentOutOfRangeException("GPA must be between 0 and 4.0");
                }
                _gpa = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Student {0} is {1} y/o, studying {2} with GPA {3}.", Name, Age, Program, GPA);
        }
    }

    class Teacher : Person
    {
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            this.Field = field;
            this.YOE = yoe;
        }

        private string _field;
        private int _yoe;

        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2 || 100 < value.Length)
                {
                    throw new ArgumentOutOfRangeException("Field name must be between 2 and 100 characters long");
                }
                _field = value;
            }
        }

        public int YOE
        {
            get
            {
                return _yoe;
            }
            set
            {
                if (value < 0 || 100 < value)
                {
                    throw new ArgumentOutOfRangeException("YOE must be between 0 and 100");
                }
                _yoe = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Teacher {0} is {1} y/o, teaching {2} with {3} years experience.", Name, Age, Field, YOE);
        }
    }

    class Program
    {
        const string FILE_PATH = @"..\..\..\people.txt";
        static List<Person> people = new List<Person>();

        static bool ReadPeopleDataFromFile()
        {
            try
            {
                if (!File.Exists(FILE_PATH))
                {
                    Console.WriteLine("[ERROR] File not exist\n");
                    return false;
                }

                string[] lines = File.ReadAllLines(FILE_PATH);

                foreach (string line in lines)
                {
                    try
                    {
                        string[] data = line.Split(';');

                        // Person's data length is 3, Student and Teacher's data length is 5 
                        if ((data.Length != 3) && (data.Length != 5))
                        {
                            Console.WriteLine("[ERROR] Corrupted data (Invaild data format): {0}", line);
                            continue;
                        }

                        switch (data[0])
                        {
                            case "Person":
                                if (!int.TryParse(data[2], out int age))
                                {
                                    Console.WriteLine("[ERROR] Corrupted data (Invalid age data type): {0}", line);
                                }
                                people.Add(new Person(data[1], age));
                                break;

                            case "Teacher":
                                if (!int.TryParse(data[2], out age))
                                {
                                    Console.WriteLine("[ERROR] Corrupted data (Invalid age data type): {0}", line);
                                    break;
                                }
                                if (!int.TryParse(data[4], out int yoe))
                                {
                                    Console.WriteLine("[ERROR] Corrupted data (Invalid YOE data type): {0}", line);
                                    break;
                                }
                                people.Add(new Teacher(data[1], age, data[3], yoe));
                                break;

                            case "Student":
                                if (!int.TryParse(data[2], out age))
                                {
                                    Console.WriteLine("[ERROR] Corrupted data (Invalid age data type): {0}", line);
                                    break;
                                }
                                if (!double.TryParse(data[4], out double gpa))
                                {
                                    Console.WriteLine("[ERROR] Corrupted data (Invalid GPA data type): {0}", line);
                                    break;
                                }
                                people.Add(new Student(data[1], age, data[3], gpa));
                                break;

                            default:
                                Console.WriteLine("[ERROR] Corrupted data (Invalid object type): {0}", line);
                                break;
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Fail to read file. {0}", ex.Message);
                return false;
            }
            return true;
        }

        static void PrintStudent()
        {
            Console.WriteLine("\n-- Student List");
            foreach (Person p in people)
            {
                if (p is Student)
                {
                    Console.WriteLine(p);
                }
            }
            Console.WriteLine();
        }

        static void PrintTeacher()
        {
            Console.WriteLine("\n-- Teacher List");
            foreach (Person p in people)
            {
                if (p is Teacher)
                {
                    Console.WriteLine(p);
                }
            }
            Console.WriteLine();
        }

        static void PrintPersonOnly()
        {
            Console.WriteLine("\n-- Only Person List");
            foreach (Person p in people)
            {
                if (p.GetType() == typeof(Person))
                {
                    Console.WriteLine(p);
                }
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            try
            {
                if (ReadPeopleDataFromFile())
                {
                    PrintStudent();
                    PrintTeacher();
                    PrintPersonOnly();
                }
            }
            finally
            {
                Console.WriteLine("Press any key to EXIT");
                Console.ReadKey();
            }
        }
    }
}
