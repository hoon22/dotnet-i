﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07SayHello
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool CheckValidation()
        {
             if (!Regex.IsMatch(tbName.Text, @"^[^;]{2,30}$"))
            {
                MessageBox.Show("[ERROR] Name should be 2-30 charaters long, not allowed to use ;");
                return false;
            }

            if ((!int.TryParse(tbAge.Text, out int age)) || age < 1 || 150 < age)
            {
                MessageBox.Show("[ERROR] Invalid data, Age should be 1-150.");
                return false;
            }
/*
            if (age < 1 || 150 < age)
            {
                MessageBox.Show("[ERROR] Age should be 1-150.");
                return false;
            }
*/
            return true;
        }

        private void ButtonName_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValidation()) return;

            MessageBox.Show(string.Format("Hello {0}, you are {1} y/o", tbName.Text, tbAge.Text));
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValidation()) return;

            const string FILE_NAME = @"..\..\..\people.txt";
            string data = string.Format("{0};{1}{2}", tbName.Text, tbAge.Text, Environment.NewLine);

            File.AppendAllText(FILE_NAME, data);
            MessageBox.Show("Data was saved succefully");

            tbName.Text = "";
            tbAge.Text = "";           
        }
    }
}
