﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3People
{
    class Person
    {
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        private string _name;
        private int _age;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if ((value < 0) || (150 < value))
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150.");
                }
                _age = value;
            }
        }
    }

    class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            this.Program = program;
            this.GPA = gpa;
        }

        private string _program;
        private double _gpa;

        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 2 || 100 < value.Length)
                {
                    throw new ArgumentOutOfRangeException("Program name must be between 2 and 100 characters long");
                }
                _program = value;
            }
        }

        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0 || 4.0 < value)
                {
                    throw new ArgumentOutOfRangeException("GPA must be between 0 and 4.0");
                }
                _gpa = value;
            }
        }
    }

    class Teacher : Person
    {
        public Teacher(string name, int age, string field, int yoe) : base (name, age)
        {
            this.Field = field;
            this.YOE = yoe;
        }

        private string _field;
        private int _yoe;

        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2 || 100 < value.Length)
                {
                    throw new ArgumentOutOfRangeException("Field name must be between 2 and 100 characters long");
                }
                _field = value;
            }
        }

        public int YOE
        {
            get
            {
                return _yoe;
            }
            set
            {
                if (value < 0 || 100 < value)
                {
                    throw new ArgumentOutOfRangeException("YOE must be between 0 and 100");
                }
                _yoe = value;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // using an initializer instead of a constructor
                // it should be public
                //Person p = new Person() { Name = "Jerry", Age = 33 };
                Person p = new Person("Jerry", 33);
                Student s = new Student("James", 36, "Dotnet", 3.7);
                Teacher t = new Teacher("Steph", 37, "IT", 3);

//                p.Name = "Maria";
//                p.Age = 11;
                Console.WriteLine("Person: {0} is {1} y/o", p.Name, p.Age);
                Console.WriteLine("Student: {0} is {1} y/o. Program: {2}, GPA: {3}", s.Name, s.Age, s.Program, s.GPA);
                Console.WriteLine("Teacher: {0} is {1} y/o. Field: {2}, YOE: {3}", t.Name, t.Age, t.Field, t.YOE);

            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                Console.WriteLine("Press any key to finish");
                Console.ReadKey();
            }
        }
    }
}
