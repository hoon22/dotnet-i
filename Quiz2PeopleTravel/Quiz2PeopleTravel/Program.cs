﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2PeopleTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string message) : base(message) { }
    }

    class PersonTravels
    {
        public PersonTravels(string name, int age, string passportNo)
        {
            Name = name;
            Age = age;
            PassportNo = passportNo;
        }

        private string _name;
        public string Name    // 2-50 characters long
        {
            get { return _name; }
            set
            {
                if (value.Length < 2 || 50 < value.Length)
                {
                    throw new InvalidDataException("[ERROR] in setter, Name should be 2-50 characters long: " + value);
                }
                _name = value;
            }
        }

        private int _age;
        public int Age     // 0-150
        {
            get { return _age; }
            set
            {
                if (value < 0 || 150 < value)
                {
                    throw new InvalidDataException("[ERROR] in setter, Age should be 0-150 y/o: " + value);
                }
                _age = value;
            }
        }

        private string _passportNo;
        public string PassportNo    // two uppercase letters followed by six digits exactly - use regexp
        {
            get { return _passportNo; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{2}[0-9]{6}$"))
                {
                    throw new InvalidDataException("[ERROR] in setter, Invalid Password Number format: " + value);
                }
                _passportNo = value;
            }
        }

        public List<string> CountriesVisited = new List<string>();

        public void AddCountryUnique(string country)
        {
            if (country == "") return;

            foreach (string c in CountriesVisited)
            {
                // check duplicated country in the list
                if (c == country)
                {
                    throw new InvalidDataException("[ERROR] in setter, Duplicated country");
                }
            }
            CountriesVisited.Add(country);
        }

        public override string ToString()
        {
            string resultStr = string.Format("{0} is {1} y / o passport {2} visited", Name, Age, PassportNo);

            string visitedCountries = "";
            for (int i=0; i < CountriesVisited.Count; i++)
            {
                visitedCountries += ((i == 0) ? "" : ", ") + CountriesVisited[i];
            }

            if (visitedCountries == "")
            {
                resultStr += " no countries";
            }
            else
            {
                resultStr += ": " + visitedCountries;
            }
            return resultStr;
        }
    }

    class Program
    {
        // data file path and name
        const string FILE_NAME = @"..\..\..\data.txt";

        // PersonTravels' container
        static List<PersonTravels> peopleTravels = new List<PersonTravels>();

        // Display user command menu
        static void DisplayMenu()
        {
            Console.WriteLine(
                "\n1. Display all people and countries they traveled to\n" +
                "2. Add person and countries visited\n" +
                "3. Register a new country visited by a person\n" +
                "4. Find out who visited most countries\n" +
                "5. List all who visited a specific country\n" +
                "0. Exit"
            );
            Console.Write("Your choice: ");
        }

        // if command is not integer, return -1. otherwise return value.
        static int GetUserCommand()
        {
            DisplayMenu();

            if (!int.TryParse(Console.ReadLine(), out int command))
            {
                return -1;  // error
            }
            return command;
        }

        static void ReadAllTravelInfoFromFile()
        {
            // check file is exist
            if (!File.Exists(FILE_NAME)) return;

            try
            {
                string[] lineArray = File.ReadAllLines(FILE_NAME);
                foreach (string line in lineArray)
                {
                    try
                    {
                        string[] data = line.Split(';');

                        // fixed a bug. April 12, 2019
                        // valid data length: 3 or 4 
                        // if ((data.Length < 3) || (data.Length > 4))
                        // valid length is always 4 
                        if (data.Length != 4)
                        {
                            Console.WriteLine("[ERROR] Corrupted data: {0}", line);
                            continue;
                        }

                        string name = data[0];

                        // convert age string to int
                        if (!int.TryParse(data[1], out int age))
                        {
                            Console.WriteLine("[ERROR] Corrupted data, Invalid data type of age: {0}", line);
                            continue;
                        }

                        string passportNo = data[2];

                        // create PersonTravels object and add to list
                        PersonTravels person = new PersonTravels(name, age, passportNo);
                        peopleTravels.Add(person);

                        // Fixed a bug. April 12, 2019
                        // Person has no visited country
                        // if (data.Length == 3) continue;

                        // Person has some visited countries.                            
                        string[] countryArray = data[3].Split(',');

                        foreach (string country in countryArray)
                        {
                            person.AddCountryUnique(country);
                        }
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("{0}: {1}", ex.Message, line);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Fail to read data from file. " + ex.Message);
            }
        }

        // 1. Display all people and countries they traveled to
        static void DisplayAllPeople()
        {
            Console.WriteLine("\nList of all people:");
            for (int i=0; i < peopleTravels.Count; i++)
            {
                Console.WriteLine("{0}. {1}", i+1, peopleTravels[i]);
            }
        }

        // 2. Add person and countries visited
        static void AddPersonAndTravelInfo()
        {
            try
            {
                Console.WriteLine("\nAdding a person and countries visited");

                Console.Write("Enter name: ");
                string name = Console.ReadLine();

                Console.Write("Enter age: ");
                if (!int.TryParse(Console.ReadLine(), out int age))
                {
                    // age must be integer
                    Console.WriteLine("[ERROR] Age should be an integer number.");
                    return;
                }

                Console.Write("Enter passport Number: ");
                string passportNo = Console.ReadLine();

                // create PersonTravels object to keep information
                PersonTravels person = new PersonTravels(name, age, passportNo);

                Console.WriteLine("Enter countries visited, one per line, enter (empty line) ends list:");
                while (true)
                {
                    string inputStr = Console.ReadLine();
                    if (inputStr == "") break;

                    // save country to the list of countries
                    person.AddCountryUnique(inputStr);
                };

                // Add person travel information to the List
                peopleTravels.Add(person);
                Console.WriteLine("Person {0} added successfully:\n{1}", person.Name, person);
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // 3. Register a new country visited by a person
        static void RegisterNewCountryVisitedByPerson()
        {
            try
            {
                Console.WriteLine("\nRegistering person visiting a new country.");
                DisplayAllPeople();

                Console.Write("\nEnter person number to add country to: ");
                if (!int.TryParse(Console.ReadLine(), out int numberOfPerson))
                {
                    Console.WriteLine("[ERROR] Invalid index number");
                    return;      // fixed a bug. April 12, 2019
                }

                // Check whether index is valid or not
                // Fixed a bug. April 12, 2019
                if (numberOfPerson <= 0 || peopleTravels.Count < numberOfPerson)
                {
                    Console.WriteLine("[ERROR] Invalid index number");
                    return;
                }

                Console.Write("Enter country name: ");
                string newCountry = Console.ReadLine();

                if (newCountry == "")
                {
                    Console.WriteLine("[ERROR] Invalid Country Name: Not allowed empty value");
                    return;
                }
                peopleTravels[numberOfPerson - 1].AddCountryUnique(newCountry);
                Console.WriteLine("Country addedd successfully.");

                Console.WriteLine(peopleTravels[numberOfPerson - 1]);
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine("Error: Country already in the list");
            }
        }

        // 4. Find out who visited most countries
        static void FindPersonVisitedMostCountries()
        {
            PersonTravels personVisitedMostCountry = null;
            int numberOfCountryVisited = -1;    // initial value

            foreach (PersonTravels p in peopleTravels)
            {
                if (p.CountriesVisited.Count > numberOfCountryVisited)
                {
                    numberOfCountryVisited = p.CountriesVisited.Count;
                    personVisitedMostCountry = p;
                }
            }

            Console.WriteLine("\nPerson who visited most countries is Marianna with {0} countries visited:", numberOfCountryVisited);
            Console.WriteLine(personVisitedMostCountry);
        }

        // 5. List all who visited a specific country
        static void ListAllPersonSpecifiedByCountry()
        {
            Console.WriteLine("\nSearching for all who visited a country");
            Console.Write("Enter country name: ");

            string country = Console.ReadLine();

            Console.WriteLine("The following people visited France:");
            foreach (PersonTravels p in peopleTravels)
            {
                // check country is in the list of countries
                if (p.CountriesVisited.Contains(country))
                {
                    Console.WriteLine(p);
                }
            }
        }

        static void SaveAllTravelInfoToFile()
        {
            List<string> peopleTravelList = new List<string>();

            // Create string list to save data
            foreach (PersonTravels p in peopleTravels)
            {
                /*
                string countryVisitedStr = "";
                for (int i=0; i < p.CountriesVisited.Count; i++)
                {
                    countryVisitedStr += ((i == 0) ? "" : ",") + p.CountriesVisited[i];
                }
                */

                // Simplified using string.Join method
                string countryVisitedStr = string.Join(",", p.CountriesVisited);

                peopleTravelList.Add(string.Format("{0};{1};{2};{3}", p.Name, p.Age, p.PassportNo, countryVisitedStr));
            }

            // Save data to file.
            try
            {
                File.WriteAllLines(FILE_NAME, peopleTravelList);
            }
            catch (IOException ex)
            {
                Console.WriteLine("[ERROR] Fail to save data into file. " + ex.Message);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // Read data from file
                ReadAllTravelInfoFromFile();

                int command;
                do
                {
                    command = GetUserCommand();
                    switch (command)
                    {
                        case 0:
                            break;
                        case 1:
                            DisplayAllPeople();
                            break;
                        case 2:
                            AddPersonAndTravelInfo();
                            break;
                        case 3:
                            RegisterNewCountryVisitedByPerson();
                            break;
                        case 4:
                            FindPersonVisitedMostCountries();
                            break;
                        case 5:
                            ListAllPersonSpecifiedByCountry();
                            break;
                        default:
                            Console.WriteLine("\n[ERROR]Invalid command try again: {0}\n", command);
                            break;
                    }
                } while (command != 0);

                // Save result
                SaveAllTravelInfoToFile();
                Console.WriteLine("\nSaving data back to file. Exiting.\n");
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
