﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool? isGameStarted = false;    // true: playing game, false: stop game, null: finished game and can restart

        private bool isTurnPlayerO = true;      // true: PlayerO, flase: PlayerX

        private Button[,] btnArray = new Button[3, 3];
        private bool?[,] btnStatusArray = new bool?[3, 3];

        // drawing background
        Brush defaultBackground;

        public MainWindow()
        {
            InitializeComponent();
            defaultBackground = lblPlayerO.Background;

            btnArray[0, 0] = bt00;
            btnArray[0, 1] = bt01;
            btnArray[0, 2] = bt02;
            btnArray[1, 0] = bt10;
            btnArray[1, 1] = bt11;
            btnArray[1, 2] = bt12;
            btnArray[2, 0] = bt20;
            btnArray[2, 1] = bt21;
            btnArray[2, 2] = bt22;
        }

        private bool IsWin(bool isPlayerO)
        {
            // check first row
            if ((btnStatusArray[0, 0] == isPlayerO && btnStatusArray[0, 1] == isPlayerO && btnStatusArray[0, 2] == isPlayerO) ||    // first row
                (btnStatusArray[1, 0] == isPlayerO && btnStatusArray[1, 1] == isPlayerO && btnStatusArray[1, 2] == isPlayerO) ||    // second row
                (btnStatusArray[2, 0] == isPlayerO && btnStatusArray[2, 1] == isPlayerO && btnStatusArray[2, 2] == isPlayerO) ||    // third row
                (btnStatusArray[0, 0] == isPlayerO && btnStatusArray[1, 0] == isPlayerO && btnStatusArray[2, 0] == isPlayerO) ||    // first column
                (btnStatusArray[0, 1] == isPlayerO && btnStatusArray[1, 1] == isPlayerO && btnStatusArray[2, 1] == isPlayerO) ||    // second column
                (btnStatusArray[0, 2] == isPlayerO && btnStatusArray[1, 2] == isPlayerO && btnStatusArray[2, 2] == isPlayerO) ||    // third column
                (btnStatusArray[0, 0] == isPlayerO && btnStatusArray[1, 1] == isPlayerO && btnStatusArray[2, 2] == isPlayerO) ||    // cross 1
                (btnStatusArray[0, 2] == isPlayerO && btnStatusArray[1, 1] == isPlayerO && btnStatusArray[2, 0] == isPlayerO))      // cross 2
            {
                return true;
            }
            return false;
        }

        private bool IsNoEmptySpace()
        {
            foreach (bool? status in btnStatusArray)
            {
                if (status == null) return false;
            }
            return true;
        }

        private void TbPlayer_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbPlayerO.Text != "" && tbPlayerX.Text != "")
            {
                btStartStop.IsEnabled = true;
                return;
            }
            btStartStop.IsEnabled = false;
        }

        private void BtStartStop_Click(object sender, RoutedEventArgs e)
        {
            if (isGameStarted == false || isGameStarted == null)
            {
                btStartStop.Content = "Stop";
                isGameStarted = true;
                isTurnPlayerO = true;
                tbPlayerO.IsEnabled = false;
                tbPlayerX.IsEnabled = false;
            }
            else
            {
                // isGameStarted == true
                isGameStarted = false;
                btStartStop.Content = "Start";
                tbPlayerO.IsEnabled = true;
                tbPlayerX.IsEnabled = true;
            }

            for (int i = 0; i < btnArray.GetLength(0); i++)
            {
                for (int j = 0; j < btnArray.GetLength(1); j++)
                {
                    btnArray[i, j].IsEnabled = (isGameStarted == true ? true : false);
                    btnArray[i, j].Content = "";
                    btnStatusArray[i, j] = null;

                    if (isGameStarted == false)
                    {
                        lblPlayerO.Background = defaultBackground;
                        lblPlayerX.Background = defaultBackground;
                    }
                    else
                    {
                        lblPlayerO.Background = Brushes.Yellow;
                        lblPlayerX.Background = defaultBackground;
                    }
                }
            }
        }

        private void BtAny_Click(object sender, RoutedEventArgs e)
        {
            if (isGameStarted == null) return;     // prevent work after finishing game.

            for (int i = 0; i < btnArray.GetLength(0); i++)
            {
                for (int j = 0; j < btnArray.GetLength(1); j++)
                {
                    if ((btnStatusArray[i, j] == null) && (btnArray[i, j] == (Button)sender))
                    {
                        // update status array and board view
                        btnStatusArray[i, j] = isTurnPlayerO;
                        btnArray[i, j].Content = isTurnPlayerO ? "O" : "X";

                        if (IsWin(isTurnPlayerO))
                        {
                            // Game finished
                            string resultStr;
                            if (isTurnPlayerO)
                            {
                                // Player O win
                                resultStr = string.Format("{0} won the tic-tac-toe game against {1}", tbPlayerO.Text, tbPlayerX.Text);
                            }
                            else
                            {
                                // Player X win
                                resultStr = string.Format("{0} won the tic-tac-toe game against {1}", tbPlayerX.Text, tbPlayerO.Text);
                            }
                            MessageBox.Show(resultStr);
                            isGameStarted = null;
                            btStartStop.Content = "Restart";
                            return;
                        }

                        // check if there are empty space.
                        if (IsNoEmptySpace())
                        {
                            MessageBox.Show("The game ended in a draw");
                            isGameStarted = null;
                            btStartStop.Content = "Restart";
                            return;
                        }

                        // change turn
                        isTurnPlayerO = !isTurnPlayerO;

                        if (isTurnPlayerO)
                        {
                            lblPlayerO.Background = Brushes.Yellow;
                            lblPlayerX.Background = defaultBackground;
                        }
                        else
                        {
                            lblPlayerX.Background = Brushes.Yellow;
                            lblPlayerO.Background = defaultBackground;
                        }
                        return;
                    }
                }
            }
        }
    }
}
