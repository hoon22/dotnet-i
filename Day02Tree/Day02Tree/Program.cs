﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            int treeHeight;

            try
            {
                Console.Write("How big does your tree needs to be? ");
                if (!int.TryParse(Console.ReadLine(), out treeHeight))
                {
                    Console.WriteLine("Error: Please enter an integer value!");
                    return;
                }

                for (int i = 0; i < treeHeight; i++)
                {
                    // Print space character
                    for (int j = i; j < treeHeight-1 ; j++)
                    {
                        Console.Write(" ");
                    }
                    // Print "*" character
                    for (int k = 0; k < (2 * i) + 1; k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                }
            } 
            finally
            {
                Console.WriteLine("\nPress any key to exit program.");
                Console.ReadKey();
            }
        }
    }
}
