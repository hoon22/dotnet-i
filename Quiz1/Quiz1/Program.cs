﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1
{
    class Program
    {
        // Q1. GPA Calculator
        static void GPACalculator()
        {
            List<string> gradeList = new List<string>();

            Console.WriteLine("Enter letter grades, one per line:");
            while (true)
            {
                string grade = Console.ReadLine();

                if (grade == "")
                {
                    break;
                }
                gradeList.Add(grade);
            }

            double sumOfGPA = 0.0;
            Console.WriteLine("You've entered:");
            foreach (string grade in gradeList)
            {
                switch (grade)
                {
                    case "A":
                        Console.WriteLine("A which is 4.0");
                        sumOfGPA += 4.0;
                        break;
                    case "A-":
                        Console.WriteLine("A which is 3.7");
                        sumOfGPA += 3.7;
                        break;
                    case "B+":
                        Console.WriteLine("A which is 3.3");
                        sumOfGPA += 3.3;
                        break;
                    case "B":
                        Console.WriteLine("A which is 3.0");
                        sumOfGPA += 3.0;
                        break;
                    case "B-":
                        Console.WriteLine("A which is 2.7");
                        sumOfGPA += 2.7;
                        break;
                    case "C+":
                        Console.WriteLine("A which is 2.3");
                        sumOfGPA += 2.3;
                        break;
                    case "C":
                        Console.WriteLine("A which is 2.0");
                        sumOfGPA += 2.0;
                        break;
                    case "D":
                        Console.WriteLine("A which is 1.0");
                        sumOfGPA += 1.0;
                        break;
                    case "F":
                        Console.WriteLine("A which is 0.0");
                        sumOfGPA += 0.0;
                        break;
                    default:
                        Console.WriteLine("Error: Invalid grade.");
                        // terminate program
                        return;
                }
            }

            Console.WriteLine("The GPA average is: {0}", sumOfGPA / gradeList.Count);
        }

        // Q2. Fibonacci number checker
        static int FibNumber(int n)
        {
            if (n == 0)
                return 0;
            if (n == 1)
                return 1;

            return FibNumber(n - 1) + FibNumber(n - 2);
        }

        static void FibChecker() {
            int inputNumber;

            Console.Write("Enter a number to check against Fibonacci: ");
            if (!int.TryParse(Console.ReadLine(), out inputNumber))
            {
                Console.WriteLine("Error: please enter an integer(not negative number).");
                return;
            }

            if (inputNumber < 0)
            {
                Console.WriteLine("Error: please enter a positive integer number");
                return;
            }

            // compare with Fibonacci number
            bool isFibNumber = false;       // indicate found or not

            int nth = 0;
            int nthFibNumber = 0;

            while (true)
            {
                nthFibNumber = FibNumber(nth);
                if (nthFibNumber == inputNumber)
                {
                    isFibNumber = true;
                    break;
                }
                else if (nthFibNumber > inputNumber)
                {
                    isFibNumber = false;
                    break;
                }
                nth++;
            }

            if (isFibNumber)
            {
                Console.WriteLine("{0} is a Fibonacci number.", inputNumber);
            }
            else
            {
                Console.WriteLine("{0} is not a Fibonacci number.", inputNumber);
                Console.WriteLine("the next Fibonacci number is {0}.", nthFibNumber);
            }
        }

        // Question 3.
        // Read matrix from user
        static int[,] GetMatrix(int row, int column, out bool isError)
        {
            int[,] arr = new int[row, column];

            isError = false;    // set default value as false
            for (int i = 0; i < row; i++)
            {
                string userInput = Console.ReadLine();
                string[] inputNumbers = userInput.Split(new char[] { ',' });

                for (int j = 0; j < column; j++)
                {
                    if (!int.TryParse(inputNumbers[j], out arr[i, j]))
                    {
                        Console.WriteLine("Error: Invalid input type");
                        isError = true;     // error occure
                        break;
                    }
                }
                if (isError) break;
            }

            return arr;
        }

        static int GetSum(int row, int column, int[,] arr1, int[,] arr2)
        {
            int result = 0;

            for (int i = 0; i < arr1.GetLength(1); i++)
            {
                result += (arr1[row, i] * arr2[i, column]);
            }

            return result;
        }

        static void MatrixMultiplier()
        {
            int[,] arr1 = new int[2, 3];
            int[,] arr2 = new int[3, 2];
            int[,] result = new int[2, 2];

            bool isError = false;

            Console.WriteLine("Enter the first matrix 2 lines of 3 numbers, comma-separated:");
            arr1 = GetMatrix(2, 3, out isError);
            if (isError) return;

            isError = false;
            Console.WriteLine("Enter the second matrix 3 lines of 2 numbers, comma-separated:");
            arr2 = GetMatrix(3, 2, out isError);
            if (isError) return;

            // Calculate matrix
            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = GetSum(i, j, arr1, arr2);
                }
            }

            // Print result matrix
            Console.WriteLine("\nThe result of multiplication is:");
            for (int i = 0; i < result.GetLength(0); i++)
            {
                for (int j = 0; j < result.GetLength(0); j++)
                {
                    Console.Write((j == 0) ? "" : ", ");
                    Console.Write("{0}", result[i, j]);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            try
            {
                /*
                // Question 1.
                GPACalculator();

                Console.WriteLine("");
      
                // Question 2.
                FibChecker();
                */

                // Question 3.
                MatrixMultiplier();
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit program.");
                Console.ReadKey();
            }
        }
    }
}
