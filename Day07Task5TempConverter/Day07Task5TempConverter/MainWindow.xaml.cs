﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Task5TempConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private double ConvertCelsiusToFahrenheit(double celsius)
        {
            return (celsius * 9 / 5) + 32;
        }

        private double ConvertCelsiusToKelvin(double celsius)
        {
            return celsius + 273.15;
        }

        private double ConvertFahrenheitToCelsius(double fahrenheit)
        {
            return (fahrenheit - 32) * 5 / 9;
        }

        private double ConvertFahrenheitToKelvin(double fahrenheit)
        {
            return (fahrenheit - 32) * 5 / 9 + 273.15;
        }

        private double ConvertKelvinToCelsius(double kelvin)
        {
            return kelvin - 273.15;
        }

        private double ConvertKelvinToFahrenheit(double kelvin)
        {
            return (kelvin - 273.15) * 9 / 5 + 32;
        }

        private void ConvertTempToCelsius(double inputTemp)
        {
            double outputTemp;

            if (rbCelsiusInput.IsChecked == true)
            {
                // input is equal to output
                outputTemp = inputTemp;
            }
            else if (rbFahrenheitInput.IsChecked == true)
            {
                outputTemp = ConvertFahrenheitToCelsius(inputTemp);
            }
            else if (rbKelvinInput.IsChecked == true)
            {
                outputTemp = ConvertKelvinToCelsius(inputTemp);
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error");
                return;
            }
            tbOutput.Text = string.Format("{0:0.##}\u00B0C", outputTemp);
        }

        private void ConvertTempToFahrenheit(double inputTemp)
        {
            double outputTemp;

            if (rbCelsiusInput.IsChecked == true)
            {
                outputTemp = ConvertCelsiusToFahrenheit(inputTemp);
            }
            else if (rbFahrenheitInput.IsChecked == true)
            {
                // input is equal to output
                outputTemp = inputTemp;
            }
            else if (rbKelvinInput.IsChecked == true)
            {
                outputTemp = ConvertKelvinToFahrenheit(inputTemp);
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error");
                return;
            }
            tbOutput.Text = string.Format("{0:0.##}\u00B0F", outputTemp);
        }

        private void ConvertTempToKelvin(double inputTemp)
        {
            double outputTemp;

            if (rbCelsiusInput.IsChecked == true)
            {
                outputTemp = ConvertCelsiusToKelvin(inputTemp);
            }
            else if (rbFahrenheitInput.IsChecked == true)
            {
                outputTemp = ConvertFahrenheitToKelvin(inputTemp);
            }
            else if (rbKelvinInput.IsChecked == true)
            {
                // input is equal to output
                outputTemp = inputTemp;
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error");
                return;
            }
            tbOutput.Text = string.Format("{0:0.##}K", outputTemp);
        }

        void ConvertTemperature()
        {
            // input text is empty.
            if (tbInput.Text == "")
            {
                tbOutput.Text = "";
                return;
            }
            // keep sign signal
            if (tbInput.Text == "-") return;

            if (!double.TryParse(tbInput.Text, out double inputTemp))
            {
                MessageBox.Show("[ERROR] Invalid input type: " + tbInput.Text);
                tbInput.Text = "";  // clear invalid value
                return;
            }

            // other idea, change input to celsius and then convert.
/*
            double celsius;
            if (rbCelsiusOutput.IsChecked == true)
            {
                celsius = inputTemp;
            }
            else if (rbFahrenheitOutput.IsChecked == true)
            {
                celsius = ConvertFahrenheitToCelsius(inputTemp);
            }
            else if (rbKelvinOutput.IsChecked == true)
            {
                celsius = ConvertKelvinToCelsius(inputTemp);
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error!!!");
            }
*/            
            // display output
            if (rbCelsiusOutput.IsChecked == true)
            {
                ConvertTempToCelsius(inputTemp);
            }
            else if (rbFahrenheitOutput.IsChecked == true)
            {
                ConvertTempToFahrenheit(inputTemp);
            }
            else if (rbKelvinOutput.IsChecked == true)
            {
                ConvertTempToKelvin(inputTemp);
            }
            else
            {
                MessageBox.Show("[ERROR] Internal System Error!!!");
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (tbInput == null || tbOutput == null) return;

            ConvertTemperature();
        }

        // this can makes error
//        private void TbInput_KeyDown(object sender, KeyEventArgs e)
//        {
//        }

        private void TbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            ConvertTemperature();
        }
    }
}
