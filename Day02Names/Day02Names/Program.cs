﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Names
{
    class Program
    {
        static void Main(string[] args)
        {
            List<String> nameList = new List<String>();
            String name;

            while (true)
            {
                Console.Write("Please Enter a name: ");
                name = Console.ReadLine();
                if (name == "")
                {
                    break;
                }
                nameList.Add(name);
            }
            Console.WriteLine();

            nameList.ForEach(delegate (String data)
            {
                Console.WriteLine(data);
            });

            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
        }
    }
}
