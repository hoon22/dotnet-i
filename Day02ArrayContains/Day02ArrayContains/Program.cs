﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02ArrayContains
{
    class Program
    {
        static public int[] Concatenate(int[] a1, int[] a2)
        {

            int[] concateArray = new int[a1.Length + a2.Length];

            a1.CopyTo(concateArray, 0);
            a2.CopyTo(concateArray, a1.Length);

            return concateArray;
        }

        static public void PrintIntArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(((i == 0) ? "" : ", ") + arr[i]);
            }
            Console.WriteLine();
        }

        static public void Print2DIntArray(int[,] arr2D)
        {
            for (int i = 0; i <= arr2D.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= arr2D.GetUpperBound(1); j++)
                {
                    Console.Write(((j == 0) ? "" : ", ") + arr2D[i, j]);
                }
                Console.WriteLine();
            }
        }

        public static bool IsElement(int[] arr, int key)
        {
            foreach (int value in arr)
            {
                if (value == key)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsElement(int[,] arr, int key)
        {
            foreach (int value in arr)
            {
                if (value == key)
                {
                    return true;
                }
            }
            return false;
        }

        public static int[] RemoveDups(int[] a1, int[] a2)
        {
            int[] tempArr = new int[a1.Length];
            int tempArrCount = 0;

            foreach (var item in a1)
            {
                if (!IsElement(a2, item))
                {
                    tempArr[tempArrCount++] = item;
                }
            }

            int[] resultArr = new int[tempArrCount];

            System.Array.Copy(tempArr, resultArr, tempArrCount);

            return resultArr;
        }


        public static void PrintDups(int[] a1, int[] a2)
        {
            // elements in array a1
            foreach (var value in a1)
            {
                if (IsElement(a2, value))
                {
                    Console.WriteLine(value);
                }
            }

            // elements in array a2
            foreach (var value in a2)
            {
                if (IsElement(a1, value))
                {
                    Console.WriteLine(value);
                }
            }
        }

        public static void PrintDups(int[,] a1, int[,] a2)
        {
            // elements in array a1
            foreach (var value in a1)
            {
                if (IsElement(a2, value))
                {
                    Console.WriteLine(value);
                }
            }

            // elements in array a2
            foreach (var value in a2)
            {
                if (IsElement(a1, value))
                {
                    Console.WriteLine(value);
                }
            }
        }


        static void Main(string[] args)
        {
            int[] array1 = { 1, 3, 7, 8, 2, 7, 9, 11 };
            int[] array2 = { 3, 8, 7, 5, 13, 5, 12 };

            // Part 1.
            Console.Write("Array 1: ");
            PrintIntArray(array1);

            Console.Write("Array 2: ");
            PrintIntArray(array2);

            Console.Write("\nConcatenate Array: ");
            PrintIntArray(Concatenate(array1, array2));

            // Part 2.
            Console.WriteLine("\nDuplicate elements: ");
            PrintDups(array1, array2);

            // Part 3.
            Console.Write("\nRemove elements duplicated: ");
            PrintIntArray(RemoveDups(array1, array2));

            // Part 4.
            int[,] arr1TwoD = 
            {
                {1, 3, 4},
                {2, 4, 5}
            };
            int[,] arr2TwoD =
            {
                {7, 3},
                {4, 8}
            };

            Console.WriteLine("\n2D Array1:");
            Print2DIntArray(arr1TwoD);

            Console.WriteLine("2D Array2:");
            Print2DIntArray(arr2TwoD);

            Console.WriteLine("\nDuplicate elements: ");
            PrintDups(arr1TwoD, arr2TwoD);


            Console.WriteLine("\nPress any key to exit program.");
            Console.ReadKey();
        }
    }
}
