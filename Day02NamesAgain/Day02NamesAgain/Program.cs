﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02NamesAgain
{
    class Program
    {
        static void Main(string[] args)
        {
            const int length = 5;
            String[] nameList = new String[length];
            String searchString;

            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter a name: ");
                nameList[i] = Console.ReadLine();
            }
            Console.Write("\nEnter search string: ");
            searchString = Console.ReadLine();

            String longestString = nameList[0];
            for (int i = 0; i < length; i++)
            {
                if (nameList[i].Contains(searchString))
                {
                    Console.WriteLine("Matching name: {0}", nameList[i]);
                }
                if (longestString.Length < nameList[i].Length)
                {
                    longestString = nameList[i];
                }
            }
            Console.WriteLine("\nLongest name is {0}", longestString);

            Console.WriteLine("\nPress any key to exit program.");
            Console.ReadKey();
        }
    }
}
