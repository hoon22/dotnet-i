﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleListInFile
{
    class Person
    {
        public Person(string name, int age, string city)
        {
            this.Name = name;
            this.Age = age;
            this.City = city;
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if ((value.Length < 2) || (100 < value.Length))
                {
                    throw new ArgumentOutOfRangeException("Name must be 2-100 characters long");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if ((value < 0) || (150 < value))
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150.");
                }
                _age = value;
            }
        }

        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if ((value.Length < 2) || (100 < value.Length))
                {
                    throw new ArgumentOutOfRangeException("City must be 2-100 characters long");
                }
                _city = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} is {1} from {2}", this.Name, this.Age, this.City);
        }

        public string GetSaveDataString()
        {
            return string.Format("{0};{1};{2}", this.Name, this.Age, this.City);
        }
    }


    class Program
    {
        private const string data_file = @"..\..\..\people.txt";
        // List of Person
        static List<Person> people = new List<Person>();

        static void DisplayMenu()
        {
            Console.WriteLine(
                "What do you want to do?\n" +
                "1. Add person info\n" +
                "2. List persons info\n" +
                "3. Find a person by name\n" +
                "4. Find all persons younger than age\n" +
                "0. Exit"
            );
            Console.Write("Choice: ");
        }

        // if command is invalid, return -1. otherwise return value 0 - 4
        static int GetUserCommand()
        {
            DisplayMenu();

            if (!int.TryParse(Console.ReadLine(), out int command))
            {
                return -1;
            }

            if ((command < 0) || (4 < command))
            {
                return -1;
            }

            return command;
        }

        static void AddPersonInfo()
        {
            Console.WriteLine("\nAdding a person.");

            Console.Write("Enter name: ");
            string name = Console.ReadLine();

            Console.Write("Enter age: ");
            string ageStr = Console.ReadLine();
            if (!int.TryParse(ageStr, out int age))
            {
                Console.WriteLine("Invalid data format(Age must be integer) :" + ageStr);
            }

            Console.Write("Enter city: ");
            string city = Console.ReadLine();

            people.Add(new Person(name, age, city));
            Console.WriteLine("Person added.\n");
        }

        static void ListAllPersonsInfo()
        {
            Console.WriteLine("\nListing all persons");
            foreach (Person person in people)
            {
                Console.WriteLine(person);
            }
            Console.WriteLine();
        }

        static void FindPersonByName()
        {
            Console.WriteLine("\nEnter partial person name:");
            string keyword = Console.ReadLine();

            Console.WriteLine("Matches found:");
            foreach (Person person in people)
            {
                if (person.Name.Contains(keyword))
                {
                    Console.WriteLine(person);
                }
            }
            Console.WriteLine();
        }

        static void FindPersonYoungerThan()
        {
            Console.WriteLine("\nEnter maximum age:");
            string ageStr = Console.ReadLine();
            if (!int.TryParse(ageStr, out int age))
            {
                Console.WriteLine("Invalid data format(Age must be integer): {0}", ageStr);
                return;
            }

            Console.WriteLine("Matches found:");
            foreach (Person person in people)
            {
                if (person.Age <= age)
                {
                    Console.WriteLine(person);
                }
            }
            Console.WriteLine();
        }

        static void ReadAllPeopleFromFile()
        {
            if (!File.Exists(data_file))
            {
                return;
            }

            string[] lines = File.ReadAllLines(data_file);

            foreach (string line in lines)
            {
                try
                {
                    string[] data = line.Split(';');

                    if (data.Length != 3)
                    {
                        throw new InvalidDataException("Error - Saving data corrupted: " + line);
                    }

                    string name = data[0];
                    if (!int.TryParse(data[1], out int age))
                    {
                        throw new InvalidDataException("Invalid data format(Age must be integer): " + line);
                    }
                    string city = data[2];

                    // insert data into array
                    people.Add(new Person(name, age, city));
                }
                catch (Exception ex)
                {
                    if (ex is InvalidDataException || ex is ArgumentOutOfRangeException)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        }

        static void SaveAllPeopleToFile()
        {
            string[] lines = new string[people.Count];

            for (int i = 0; i < people.Count; i++)
            {
                lines[i] = people[i].GetSaveDataString();
            }

            try
            {
                File.WriteAllLines(data_file, lines);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error during save file: " + ex.Message);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // Read data from file
                ReadAllPeopleFromFile();

                bool isDone = false;
                do
                {
                    switch (GetUserCommand())
                    {
                        case 0:
                            isDone = true;
                            break;
                        case 1:
                            AddPersonInfo();
                            break;
                        case 2:
                            ListAllPersonsInfo();
                            break;
                        case 3:
                            FindPersonByName();
                            break;
                        case 4:
                            FindPersonYoungerThan();
                            break;
                        default:
                            Console.WriteLine("Invalid command try again.\n");
                            break;
                    }
                } while (!isDone);

                // Save result
                SaveAllPeopleToFile();
                Console.WriteLine("\nGood bye!\n");
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
