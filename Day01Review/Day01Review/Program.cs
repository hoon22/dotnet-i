﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Review
{
    class Program
    {
        static void Main(string[] args)
        {
            string userinput;

            int min;
            int max;
            int count;

            bool isInteger;
            bool isError;

            do
            {
                Console.Write("Enter minimum: ");
                userinput = Console.ReadLine();

                isInteger = int.TryParse(userinput, out min);
                if (!isInteger)
                {
                    Console.WriteLine("Error: Value entered must be an integer");
                }
            } while (!isInteger);

            do
            {
                isError = false;
                Console.Write("Enter maximum: ");
                userinput = Console.ReadLine();

                isInteger = int.TryParse(userinput, out max);
                if (!isInteger)
                {
                    Console.WriteLine("Error: Value entered must be an integer");
                }
                else if (min >= max)
                {
                    isError = true;
                    Console.WriteLine("Error: minimum can not be larger than maximum");
                }
            } while (!isInteger || isError);

            do
            {
                isError = false;
                Console.Write("Enter how many numbers to generate: ");
                userinput = Console.ReadLine();

                isInteger = int.TryParse(userinput, out count);
                if (!isInteger)
                {
                    Console.WriteLine("Error: Value entered must be an integer");
                }
                else if (count < 1)
                {
                    isError = false;
                    Console.WriteLine("Error: you must generate 1 or more numbers");
                }
            } while (!isInteger || isError);
   

            // Print the result.
            Random random = new Random();

            Console.Write("Numbers generated: ");
            for (int i=0; i<count; i++)
            {
                Console.Write((i == 0 ? "" : ", ") + $"{random.Next(min, max+1)}");
            }
            Console.WriteLine();

            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();
        }
    }
}
