﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your name: ");
            string userName = Console.ReadLine();

            Console.Write("Enter your age: ");
            if (int.TryParse(Console.ReadLine(), out int age)) {
                // age is int value
                if (age < 0)
                {
                    Console.WriteLine("Error: Age must be greater than or equal to 0.");
                }
                else
                {
                    // valid user age
                    Console.WriteLine("Hi {0}, you are {1} y/o.", userName, age);
                    if (age < 18)
                    {
                        Console.WriteLine("You are not an adult yet.");
                    }
                }
            }
            else
            {
                Console.WriteLine("Error: Age entered must be an integer.");
            }
            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}
