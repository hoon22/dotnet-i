﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleFromFile
{
    class Person
    {
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        private string _name;
        private int _age;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if ((value < 0) || (150 < value))
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150.");
                }
                _age = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}", this.Name, this.Age);
        }
    }

    class Program
    {
        static List<Person> people = new List<Person>();

        static void Main(string[] args)
        {
            try
            {
                string[] inputLines = File.ReadAllLines("../../../people.txt");

                foreach (string line in inputLines)
                {
                    try
                    {
                        string[] values = line.Split(new char[] { ';' });
                        
                        if (values.Length != 2)
                        {
                            throw new InvalidDataException(string.Format("Invalid data input: {0}", line));
                        }

                        string name = values[0];
                        if (!int.TryParse(values[1], out int age))
                        {
                            throw new TypeLoadException("Age must be integer");
                        }
                        people.Add(new Person(name, age));
                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentOutOfRangeException || ex is TypeLoadException)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                foreach (Person person in people)
                {
                    Console.WriteLine(person);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
                if (ex is ArgumentOutOfRangeException || ex is TypeLoadException)
                {
                    Console.WriteLine(ex.Message);
                }
                
            }
            finally
            {
                Console.WriteLine("\nPress any key to exit");
                Console.ReadKey();
            }
        }
    }
}
