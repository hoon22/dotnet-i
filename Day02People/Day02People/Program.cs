﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02People
{
    class Program
    {
        static void Main(string[] args)
        {
            const int countOfNames = 4;

            String[] namesArray = new String[countOfNames];
            int[] agesArray = new int[countOfNames];

            for (int i = 0; i < countOfNames; i++)
            {
                Console.Write("Enter name of person #{0}: ", i + 1);
                namesArray[i] = Console.ReadLine();

                Console.Write("Enter age  of person #{0}: ", i + 1);
                int.TryParse(Console.ReadLine(), out agesArray[i]);
            }

            int youngestPersonIndex = 0;
            for (int i = 0; i < countOfNames; i++)
            {
                if (agesArray[youngestPersonIndex] > agesArray[i])
                {
                    youngestPersonIndex = i;
                }
            }
            Console.WriteLine("\nYoungest person is {0} and their name is {1}.", 
                agesArray[youngestPersonIndex], namesArray[youngestPersonIndex]);


            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}
