﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int MAX_INPUT = 2;

        private int currentInputIndex;      // manage status, 0:1st input, 1:2nd input, 2:result

        private string[] inputValues = new string[MAX_INPUT];           // input value array
        private TextBox[] tbInputValueArray = new TextBox[MAX_INPUT];   // input TextBox

        public MainWindow()
        {
            InitializeComponent();

            // initialize input TextBox array
            tbInputValueArray[0] = tb1stValue;
            tbInputValueArray[1] = tb2ndValue;

            ResetCalculator();
        }

        private void ResetCalculator()
        {
            currentInputIndex = 0;

            for (int i = 0; i < MAX_INPUT; i++)
            {
                inputValues[i] = "";
                tbInputValueArray[i].Text = "";
            }
            tbResult.Text = "";

            tb2ndValue.IsEnabled = false;           
            tbResult.IsEnabled = false;
            // clear operation label
            lblOperation.Content = "";

            UpdateUI();
        }

        // Change enable/disable status for number input button
        private void EnableNumberInput(bool isEnabled)
        {
            btNo0.IsEnabled = isEnabled;
            btNo1.IsEnabled = isEnabled;
            btNo2.IsEnabled = isEnabled;
            btNo3.IsEnabled = isEnabled;
            btNo4.IsEnabled = isEnabled;
            btNo5.IsEnabled = isEnabled;
            btNo6.IsEnabled = isEnabled;
            btNo7.IsEnabled = isEnabled;
            btNo8.IsEnabled = isEnabled;
            btNo9.IsEnabled = isEnabled;
            btPoint.IsEnabled = isEnabled;
            btSign.IsEnabled = isEnabled;
        }

        private void UpdateUI()
        {
            switch (currentInputIndex)
            {
                case 0:
                    tb1stValue.IsEnabled = true;
                    tb2ndValue.IsEnabled = false;
                    tbResult.IsEnabled = false;
                    // label UI
                    lbl1stValue.Background = Brushes.Yellow;
                    lbl2ndValue.Background = null;
                    lblResult.Background = null;
                    // Operation UI
                    btMultiply.IsEnabled = true;
                    btAdd.IsEnabled = true;
                    btSubtract.IsEnabled = true;
                    btDivide.IsEnabled = true;
                    btResult.IsEnabled = false;
                    // number input button
                    EnableNumberInput(true);
                    break;
                case 1:
                    tb1stValue.IsEnabled = false;
                    tb2ndValue.IsEnabled = true;
                    tbResult.IsEnabled = false;
                    // label UI
                    lbl1stValue.Background = null;
                    lbl2ndValue.Background = Brushes.Yellow;
                    lblResult.Background = null;

                    btMultiply.IsEnabled = false;
                    btAdd.IsEnabled = false;
                    btSubtract.IsEnabled = false;
                    btDivide.IsEnabled = false;
                    btResult.IsEnabled = true;
                    break;
                case 2:
                    tb1stValue.IsEnabled = false;
                    tb2ndValue.IsEnabled = false;
                    tbResult.IsEnabled = true;
                    // label UI
                    lbl1stValue.Background = null;
                    lbl2ndValue.Background = null;
                    lblResult.Background = Brushes.Yellow;
                    // result(=) button
                    btResult.IsEnabled = false;
                    // number input button
                    EnableNumberInput(false);
                    break;
                default:
                    MessageBox.Show("[ERROR] Internal index value error");
                    break;
            }
        }

        private void BtNumberInput_Click(object sender, RoutedEventArgs e)
        {
            Button numberButton = (Button)sender;

            if (numberButton == btPoint)
            {
                if (inputValues[currentInputIndex].Contains("."))
                {
                    // point(.) is already part of input, so ignore that
                    return;
                }
                if (inputValues[currentInputIndex] == "")
                {
                    // Add 0 in front of point(.)
                    inputValues[currentInputIndex] = "0";
                }
                inputValues[currentInputIndex] += ".";
                // update UI component;
                tbInputValueArray[currentInputIndex].Text = inputValues[currentInputIndex];
                return;
            }

            if (numberButton == btSign)
            {
                if (inputValues[currentInputIndex].Contains("-"))
                {
                    // clear minus(-) sign
                    inputValues[currentInputIndex] = inputValues[currentInputIndex].Substring(1);
                }
                else
                {
                    inputValues[currentInputIndex] = "-" + inputValues[currentInputIndex];
                }
                // update UI component
                tbInputValueArray[currentInputIndex].Text = inputValues[currentInputIndex];
                return;
            }

            // if current value is 0, not allow to add 0 more
            if (inputValues[currentInputIndex] == "0") return;

            inputValues[currentInputIndex] += numberButton.Content.ToString();
            tbInputValueArray[currentInputIndex].Text = inputValues[currentInputIndex];
        }

        // Operation button clicked
        private void BtOperation_Click(object sender, RoutedEventArgs e)
        {
            // currentInputIndex is not first or first value is empty string, ignore
            if (currentInputIndex != 0 || inputValues[currentInputIndex] == "") return;

            // update status
            currentInputIndex++;
            // update label text
            lblOperation.Content = ((Button)sender).Content.ToString();

            UpdateUI();
        }

        private void BtResult_Click(object sender, RoutedEventArgs e)
        {
            // if current input is not second or second value is "", ignore
            if (currentInputIndex != 1 || inputValues[currentInputIndex] == "") return;

            // calculate result
            if (!double.TryParse(inputValues[0], out double value1))
            {
                MessageBox.Show("[ERROR] first value is invalid");
                return;
            }
            if (!double.TryParse(inputValues[1], out double value2))
            {
                MessageBox.Show("[ERROR] first value is invalid");
                return;
            }

            // calculate result
            double result;
            switch (lblOperation.Content.ToString())
            {
                case "+":
                    result = value1 + value2;
                    break;
                case "-":
                    result = value1 - value2;
                    break;
                case "*":
                    result = value1 * value2;
                    break;
                case "/":
                    result = value1 / value2;
                    break;
                default:
                    MessageBox.Show("[ERROR] Invalid operation " + lblOperation.Content.ToString());
                    return;
            }

            currentInputIndex++;    // update status
            tbResult.Text = string.Format("{0}", result);

            UpdateUI();

            // save result to file
            try
            {
                string resultStr = string.Format("{0} {1} {2} = {3}{4}", inputValues[0], lblOperation.Content.ToString(), inputValues[1], result, Environment.NewLine);
                File.AppendAllText("Record.txt", resultStr);
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Save file error " + ex.Message);
            }
        }

        private void BtReset_Click(object sender, RoutedEventArgs e)
        {
            // reset calculator
            ResetCalculator();
        }
    }
}
